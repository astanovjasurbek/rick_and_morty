let main = document.querySelector('.main')

let counts = 1
let arr = new Array()
let page = 0;

async function generate() {
    page++;
    if(page<43){
    let data = await fetch(`https://rickandmortyapi.com/api/character?page=${page}`)
    let data2 = await data.json()
    let date = data2.results;
    for (let i = 0; i < date.length; i++) {
            //create
            let main_info, main_img,
                newblock, name_count, div_main_info_sub, circle_count, status_count,
                newblock_2, last_known_location, planet_count,
                newblock_3, First_seen, first_seen_count
            let item = document.createElement('div')
            item.classList = 'item'
            main.insertAdjacentElement('beforeend', item)

            main_img = document.createElement('div')
            main_img.classList = `cl main_img main_img_560`
            main_img.setAttribute("id-user", `${date[i].id} im`)
            item.insertAdjacentElement('beforeend', main_img)
            document.querySelector(`[id-user="${date[i].id} im"]`).style.backgroundImage = `url(${date[i].image})`

            main_info = document.createElement('div')
            main_info.classList = 'main_info'
            item.insertAdjacentElement('beforeend', main_info)

            newblock = document.createElement('div')
            main_info.insertAdjacentElement('beforeend', newblock)

            newblock_2 = document.createElement('div')
            main_info.insertAdjacentElement('beforeend', newblock_2)

            newblock_3 = document.createElement('div')
            main_info.insertAdjacentElement('beforeend', newblock_3)

            name_count = document.createElement('p')
            name_count.classList = `cl main_info_style orange_color`
            name_count.setAttribute("id-user" , `${date[i].id} na`)
            newblock.insertAdjacentElement('beforeend', name_count)
            console.log(date[i].name);
            document.querySelector(`[id-user="${date[i].id} na"]`).innerHTML = date[i].name

            div_main_info_sub = document.createElement('div')
            div_main_info_sub.classList = `main_info_style_sub`
            newblock.insertAdjacentElement('beforeend', div_main_info_sub)

            circle_count = document.createElement('div')
            circle_count.classList = `circle circle_${date[i].id}`
            div_main_info_sub.insertAdjacentElement('beforeend', circle_count)

            status_count = document.createElement('span')
            status_count.classList = `status_${date[i].id}`
            div_main_info_sub.insertAdjacentElement('beforeend', status_count)
            document.querySelector(`.status_${date[i].id}`).innerHTML = date[i].status + '-' + date[i].species
            if (date[i].status == "Alive") {
                document.querySelector(`.circle_${date[i].id}`).style.backgroundColor = 'green'
            }
            else if (date[i].status == "Dead") {
                document.querySelector(`.circle_${date[i].id}`).style.backgroundColor = 'red'
            }
            else if (date[i].status == "unknown") {
                document.querySelector(`.circle_${date[i].id}`).style.backgroundColor = 'blue'
            }

            last_known_location = document.createElement('h4')
            last_known_location.classList = 'main_info_style_sub2'
            last_known_location.innerText = 'Last known location:'
            newblock_2.insertAdjacentElement('beforeend', last_known_location)

            planet_count = document.createElement('p')
            planet_count.classList = `cl main_info_style_sub3 orange_color`
            planet_count.setAttribute("id-user", `${date[i].id} pl` )
            newblock_2.insertAdjacentElement('beforeend', planet_count)
            document.querySelector(`[id-user="${date[i].id} pl"]`).innerHTML = Object.values(date[i].location)[0]

            First_seen = document.createElement('h4')
            First_seen.classList = 'main_info_style_sub2'
            First_seen.innerText = 'First seen in:'
            newblock_3.insertAdjacentElement('beforeend', First_seen)

            first_seen_count = document.createElement('p')
            first_seen_count.classList = `cl main_info_style_sub3 orange_color`
            first_seen_count.setAttribute("id-user", `${date[i].id} fi`)
            newblock_3.insertAdjacentElement('beforeend', first_seen_count)
            let data_2 = await fetch(date[i].episode[0])
            let date_2 = await data_2.json()
            document.querySelector(`[id-user="${date[i].id} fi"]`).innerHTML = date_2.name;
            
        }
}
}




function click() {
    document.querySelectorAll('.cl').forEach(element => {
        element.addEventListener('click', (event) => {
            const idUser = event.target.getAttribute("id-user").split(' ');
            if (idUser[1] =="na") {
                fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data => data.json()).then(date => {
                    Swal.fire(`Персонаж ${date.name}`)
                })
            }
            else if (idUser[1] =="pl") {
                fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data => data.json()).then(date => {
                    fetch(Object.values(date.location)[1]).then(data => data.json()).then(date => {
                        Swal.fire(`Впервые появляется в локации ${date.name}`)
                    })
                })
            }
            else if (idUser[1] =="fi") {
                fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data => data.json()).then(date => {
                    fetch(date.episode[0]).then(data => data.json()).then(date => {
                        Swal.fire(`Впервые появляется в эпизоде № ${date.id}  ${date.name}`)
                    })
                })
            }
            else if(idUser[1] =="im"){
                fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data => data.json()).then(date => {
                    Swal.fire(`Персонаж ${date.name}`)
                })
            }
        })
    })
}
generate().then(()=>{
    click()
})





window.addEventListener("scroll", () => {

    let contentHeight = main.offsetHeight;
    let yOffset = window.pageYOffset;
    let window_height = window.innerHeight;
    let y = yOffset + window_height;
    if (counts < 827) {
        if (y >= contentHeight - 400) {
            generate().then(()=>{click()})
        }
    }

});



//////Поиск//////////


document.querySelector('.btn').addEventListener('click', () => {
    let a = document.querySelector('.search_text').value
    if (!a.split(' ')) {
        let s = a.split(' ')
        s[0][0] = s[0][0].toUpperCase();
        s[1][0] = s[1][0].toUpperCase();
        a = s.join(' ')
    }
    else {
        a[0] = a[0].toUpperCase();
    }
    fetch(`https://rickandmortyapi.com/api/character?name=${a}`).then(data => data.json()).then(date => {
        document.querySelector('.main_img').style.backgroundImage = `url(${date.results[0].image})`
        document.querySelector('.name').innerText = date.results[0].name
        document.querySelector('.status').innerText =date.results[0].status + '-' + date.results[0].species
        if (date.results[0].status == "Alive") {
            document.querySelector(`.circle_g`).style.backgroundColor = 'green'
        }
        else if (date.results[0].status == "Dead") {
            document.querySelector(`.circle_g`).style.backgroundColor = 'red'
        }
        else if (date.results[0].status == "unknown") {
            document.querySelector(`.circle_g`).style.backgroundColor = 'blue'
        }
        document.querySelector('.planet').innerText = Object.values(date.results[0].location)[0]
        fetch(date.results[0].episode[0]).then(data_2 => data_2.json()).then(date_2 => {
            document.querySelector(`.first_seen`).innerHTML = date_2.name;
        })

        document.querySelector('.search_none').style.display = 'block'
    })
    document.querySelector('.search_text').value = ''
})



