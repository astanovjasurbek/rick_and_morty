function generate( count=6){
    let rand_number
    let arr_numbers= new Array()
    for(let i =0; i< count; i++){
        rand_number = Math.floor(Math.random() * (826 - 0)) + 1;
        arr_numbers.push(rand_number)
    }
    arr_numbers.sort(function (a, b) {
        return a - b;
    });
    console.log(  arr_numbers);
        fetch(`https://rickandmortyapi.com/api/character/${arr_numbers}`).then(data=>data.json()).then(date=>{
            for(let i=0; i < count; i++){
                document.querySelector(`.main_img_${i+1}`).setAttribute("id-user",`${arr_numbers[i]} im`)
                document.querySelector(`.main_img_${i+1}`).style.backgroundImage=`url(${date[i].image})`
                document.querySelector(`.name_${i+1}`).setAttribute("id-user",`${arr_numbers[i]} na`)
                document.querySelector(`.name_${i+1}`).innerHTML=date[i].name
                document.querySelector(`.status_${i+1}`).innerHTML=date[i].status +'-'+ date[i].species
                if(date.status=="Alive"){
                    document.querySelector(`.circle_${i+1}`).style.backgroundColor = 'green'
                }
                else if(date.status=="Dead"){
                    document.querySelector(`.circle_${i+1}`).style.backgroundColor = 'red'
                }
                else if(date.status=="unknown"){
                  
                    document.querySelector(`.circle_${i+1}`).style.backgroundColor = 'blue'
                }
                document.querySelector(`.planet_${i+1}`).setAttribute("id-user",`${arr_numbers[i]} pl`)
                document.querySelector(`.planet_${i+1}`).innerHTML=Object.values(date[i].location)[0]
                fetch(date[i].episode[0]).then(data_2=>data_2.json()).then(date_2=>{
                    document.querySelector(`.first_seen_${i+1}`).setAttribute("id-user",`${arr_numbers[i]} fi`)
                    document.querySelector(`.first_seen_${i+1}`).innerHTML=date_2.name;
                })
            }
        })
}
generate();

window.onkeydown=(event)=>{
    if(event.ctrlKey && event.key == 's'){
        event.preventDefault()
        generate()
    }
}


document.querySelectorAll('.cl').forEach(element => {
    element.addEventListener('click' , (event)=>{
        const idUser = event.target.getAttribute("id-user").split(' ');
        if(idUser[1] =="na"){
            window.open(`https://rickandmortyapi.com/api/character/${idUser[0]}`)
        }
        else if(idUser[1] =="pl"){
            fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data=>data.json()).then(date=>{
                window.open(Object.values(date.location)[1])
            })
        }
        else if(idUser[1] =="fi"){
            fetch(`https://rickandmortyapi.com/api/character/${idUser[0]}`).then(data=>data.json()).then(date=>{
               window.open(date.episode[0])
            })
        }   
        else if(idUser[1] =="im"){
            window.open(`https://rickandmortyapi.com/api/character/${idUser[0]}`)
        }
    })
})

